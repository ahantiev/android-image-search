package com.ahantiev.imageserach;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ahantiev.imagesearch.R;
import com.ahantiev.imageserach.adapter.PixabayImageAdapter;
import com.ahantiev.imageserach.model.PixabayResponse;
import com.ahantiev.imageserach.service.PixabayImageSearchService;
import com.ahantiev.imageserach.service.ServiceFactory;
import com.etsy.android.grid.StaggeredGridView;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ImageSearchFragment extends Fragment {

    private static final String TAG = "ImageSearchFragment";

    private PixabayImageSearchService imageSearchService;

    private TextView mSearchText;
    private StaggeredGridView mGridView;
    private PixabayImageAdapter mAdapter;
    private CompositeSubscription compositeSubscription;
    private ProgressBar mProgress;
    private TextView mContentNotFoundText;
    private EndlessScrollListener endlessScrollListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_grid, container, false);
        init(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
        super.onDestroyView();
    }

    private void init(View view) {
        imageSearchService = ServiceFactory.createRetrofitService(PixabayImageSearchService.class,
                PixabayImageSearchService.PIXABAY_API_ENDPOINT);

        mGridView = (StaggeredGridView) view.findViewById(R.id.grid_view);
        mSearchText = (TextView) view.findViewById(R.id.search_text);
        mContentNotFoundText = (TextView) view.findViewById(R.id.content_not_found_text);
        mProgress = (ProgressBar) view.findViewById(R.id.indicator);
        mProgress.setVisibility(View.GONE);

        if (this.compositeSubscription == null || compositeSubscription.isUnsubscribed()) {
            this.compositeSubscription = new CompositeSubscription(
                    searchTextChangeSubscription()
            );
        }

        if (mAdapter == null) {
            mAdapter = new PixabayImageAdapter(view.getContext(), R.layout.grid_item_layout);
        }

        mGridView.setAdapter(mAdapter);

        endlessScrollListener = new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                loadMoreImages(page);
                return true;
            }
        };
        mGridView.setOnScrollListener(endlessScrollListener);

        checkThatContentIsNotFound();
    }

    private Subscription searchTextChangeSubscription() {
        return RxTextView.textChanges(mSearchText)
                .skip(1)
                .doOnNext(query -> {
                    mAdapter.clear();
                    if (query.length() != 0) {
                        mProgress.setVisibility(View.VISIBLE);
                        mContentNotFoundText.setVisibility(View.GONE);
                    } else {
                        mProgress.setVisibility(View.GONE);
                        mContentNotFoundText.setVisibility(View.VISIBLE);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(query -> query.length() != 0)
                .flatMap(query -> getImages(query, 1))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(handleResponse(), handleError());
    }

    private void loadMoreImages(int page) {
        mProgress.setVisibility(View.VISIBLE);
        Subscription loadMoreImagesSubscription = getImages(mSearchText.getText().toString(), page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(handleResponse(), handleError());
        this.compositeSubscription.add(loadMoreImagesSubscription);
    }

    private Observable<? extends PixabayResponse> getImages(CharSequence query, int page) {
        return imageSearchService.getImages(query.toString(), page)
                .timeout(5, TimeUnit.SECONDS)
                .retry(3)
                .onErrorResumeNext(throwable -> {
                    PixabayResponse response = new PixabayResponse();
                    response.setError(true);
                    response.setErrorCause(throwable);
                    return Observable.just(response);
                });
    }

    @NonNull
    private Action1<PixabayResponse> handleResponse() {
        return response -> {
            mProgress.setVisibility(View.GONE);

            if(response.isError()) {
                checkThatContentIsNotFound();
                throw new RuntimeException("PixabayResponseException", response.getErrorCause());
            }

            mAdapter.loadItems(response);
            checkThatContentIsNotFound();
        };
    }

    @NonNull
    private Action1<Throwable> handleError() {
        return throwable -> {
            Toast.makeText(getActivity(),  R.string.error_loadig_data, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "OnError " + throwable.getMessage(), throwable);
        };
    }


    private void checkThatContentIsNotFound() {
        if (mAdapter.getCount() == 0) {
            mContentNotFoundText.setVisibility(View.VISIBLE);
        } else {
            mContentNotFoundText.setVisibility(View.GONE);
        }
    }

}
