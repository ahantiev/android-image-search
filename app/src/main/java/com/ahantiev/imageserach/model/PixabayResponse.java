package com.ahantiev.imageserach.model;

import java.util.ArrayList;

public class PixabayResponse {
    private ArrayList<PixabayImage> hits;
    private Integer total;
    private Integer totalHits;
    private boolean isError = false;
    private Throwable errorCause;

    public PixabayResponse() {
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public ArrayList<PixabayImage> getHits() {
        return hits;
    }

    public void setHits(ArrayList<PixabayImage> hits) {
        this.hits = hits;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(Integer totalHits) {
        this.totalHits = totalHits;
    }

    public Throwable getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(Throwable errorCause) {
        this.errorCause = errorCause;
    }
}