package com.ahantiev.imageserach;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.ahantiev.imagesearch.R;

public class MainActivity extends FragmentActivity  {
    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}

