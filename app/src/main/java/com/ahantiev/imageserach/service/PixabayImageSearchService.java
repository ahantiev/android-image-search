package com.ahantiev.imageserach.service;

import com.ahantiev.imageserach.model.PixabayResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface PixabayImageSearchService {
    String PIXABAY_API_ENDPOINT = "https://pixabay.com/api/";
    String API_KEY = "4602625-8872732db3875d0cf9511833a";
    Integer DEFAULT_PAGE_SIZE = 20;

    @GET("?key=" + API_KEY)
    Observable<PixabayResponse> getImages(@Query("q") String query, @Query("page") int page);
}
