package com.ahantiev.imageserach.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ahantiev.imagesearch.R;
import com.ahantiev.imageserach.model.PixabayImage;
import com.ahantiev.imageserach.model.PixabayResponse;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.squareup.picasso.Picasso;


public class PixabayImageAdapter extends ArrayAdapter<PixabayImage> {

    private static final String TAG = "PixabayImageAdapter";
    private Integer currentPage = 1;

    static class ViewHolder {
        DynamicHeightImageView imageView;
    }

    public PixabayImageAdapter(final Context context, final int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        PixabayImage item = getItem(position);
        PixabayImageAdapter.ViewHolder vh;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_item_layout, parent, false);
            vh = new PixabayImageAdapter.ViewHolder();
            vh.imageView = (DynamicHeightImageView) convertView.findViewById(R.id.image);
            convertView.setTag(vh);
        } else {
            vh = (PixabayImageAdapter.ViewHolder) convertView.getTag();
        }

        vh.imageView.setHeightRatio(item.getWebformatHeightRatio());
        vh.imageView.setImageBitmap(null);

        Picasso.with(vh.imageView.getContext())
                .load(item.getWebformatURL())
                .fit()
                .into(vh.imageView);

        return convertView;
    }

    @Override
    public void clear() {
        this.currentPage = 1;
        super.clear();
    }

    public void loadItems(PixabayResponse response) {
        if (response == null || response.getHits() == null) {
            return;
        }
        this.currentPage++;
        this.addAll(response.getHits());
    }

    public Integer getCurrentPage() {
        return currentPage;
    }
}